import axios from 'axios';

const GET_BOOK_API_BASE_URL = "http://localhost:8083/product";

class ProductService {

    getProduct(token){
        return axios.get(GET_BOOK_API_BASE_URL+"/products",
        { headers: {"Authorization" : `Bearer ${token}`} });
    }
    // getProductById(sNo){
    //     return axios.get("http://localhost:8081/product/"+sNo);
    // }
    addProduct(product){
        return axios.post(GET_BOOK_API_BASE_URL+"/products",product);
    }
    updateProduct(product){
        return axios.put(GET_BOOK_API_BASE_URL+"/product",product);
    }
    deleteProduct(id,token){
        return axios.delete(GET_BOOK_API_BASE_URL+"/product/"+id,{ headers: {"Authorization" : `Bearer ${token}`} });
    }
}
export default new ProductService();