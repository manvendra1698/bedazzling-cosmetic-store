import axios from 'axios';

const GET_AUTH_API_BASE_URL = "http://localhost:8081/authentication";

class AuthService {

    login(user){
        return axios.post(GET_AUTH_API_BASE_URL+"/login",user);
    }
    getRole(userName,password){
        return axios.get(GET_AUTH_API_BASE_URL+"/role/"+userName+"/"+password);
    }
    // // getProductById(sNo){
    // //     return axios.get("http://localhost:8081/updateproduct/"+sNo);
    // // }
    // saveProduct(product){
    //     return axios.post(GET_AUTH_API_BASE_URL+"/products",product);
    // }
    // updateProduct(product){
    //     return axios.put(GET_AUTH_API_BASE_URL+"/product",product);
    // }
    // deleteProduct(id){
    //     return axios.delete(GET_AUTH_API_BASE_URL+"/product/"+id);
    // }
}
export default new AuthService();