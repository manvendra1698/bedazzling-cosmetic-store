import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'

import RegisterUserComponent from './components/RegisterUserComponent';
import LoginUserComponent from './components/LoginUserComponent';
import ProductViewComponent from './components/ProductViewComponent';
import AdminMenuComponent from './components/AdminMenuComponent';
import UserMenuComponent from './components/UserMenuComponent';
import AddProductComponent from './components/AddProductComponent';
import UpdateProductComponent from './components/UpdateProductComponent';


function App() {
  return (
  

      
      <Router>
        <Routes> 
        <Route path = "/" index element = {<RegisterUserComponent/>}></Route>
        <Route path = "/login" element = {<LoginUserComponent/>}></Route>
        <Route path = "/products" element = {<ProductViewComponent/>}></Route>
        <Route path = "/admindashboard" element = {<AdminMenuComponent/>}></Route>
        <Route path = "/userdashboard" element = {<UserMenuComponent/>}></Route>
        <Route path = "/addProduct" element = {<AddProductComponent/>}></Route>
        <Route path = "/updateProduct/:id" element = {<UpdateProductComponent/>}></Route>

        


        
        </Routes>
        </Router>
    
  );
}

export default App;
