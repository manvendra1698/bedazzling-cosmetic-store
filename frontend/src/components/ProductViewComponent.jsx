import React, { Component } from 'react'
import AdminMenuComponent from './AdminMenuComponent';
import UserMenuComponent from './UserMenuComponent';
import './design.css';
import ProductService from '../services/ProductService';
import {useNavigate} from 'react-router-dom';

const buttonStyle = {
    marginRight: "16px",
    padding: "5px 17px 5px 17px"
}

class ProductViewComponent extends Component {

    constructor(props) {
        super(props)
        
         this.state = {
            products: []
        }
        this.addProduct = this.addProduct.bind(this);
        this.updateProduct = this.updateProduct.bind(this);
        this.deleteProduct = this.deleteProduct.bind(this);
    }

    componentDidMount(){
       //console.log(sessionStorage.getItem("token"));
     ProductService.getProduct(sessionStorage.getItem("token")).then(res=>{
        this.setState({ products: res.data});
     
       });
       
        
    }

    addProduct() {
       this.props.navigate('../addProduct');
    }

    updateProduct(id){
        this.props.navigate(`../updateProduct/${id}`);
    }
    deleteProduct=(id)=>{
        ProductService.deleteProduct(id,sessionStorage.getItem("token")).then(res=>(res.data))
    }

    render(){
        return(
            <div class="view-product">
                      
                        <br/>
                     <div>
                       <AdminMenuComponent/>
                       <span><button style={buttonStyle} className="btn btn-success add-button" onClick={() => this.addProduct()} >Add Product </button></span>
                        <table class= "table table-striped table-light table-bordered">

                            <thead>
                                <tr>
                                    <th> Product ID</th>
                                    <th> Product Name</th>
                                    <th> Product Type</th>
                                    <th> Product Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.products.map(
                                        product => 
                                        <tr key = {product.productId}>
                                            <td>{product.productId}</td>
                                            <td> {product.productName} </td>
                                            <td> {product.productType}</td>
                                             <td> {product.productPrice} </td>   
                                             <td><span><button style={buttonStyle} className="btn btn-success" onClick={() => this.updateProduct(product.productId)} >Update </button></span>
                                             <span><button style={buttonStyle} className="btn btn-danger" onClick={() => this.deleteProduct(product.productId)} >Delete </button></span></td>
                                             
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
        );
    }

}
function WithNavigate(props) {
    let navigate = useNavigate();
    return <ProductViewComponent {...props} navigate={navigate} />
  }
  export default WithNavigate;
  
