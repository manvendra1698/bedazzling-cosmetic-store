import React, { Component } from 'react'
import {useNavigate} from 'react-router-dom';
class UserMenuComponent extends Component {

    useLogOutButton = (p) => {
        p.preventDefault();
        //this.props.history.push('/');
        this.props.navigate('/login');
    }

   
    viewProducts =(p)=>{
        p.preventDefault();
       //this.props.history.push('/books');
       this.props.navigate('/products');
    }
    userDashboardButton = (p) => {
        p.preventDefault();
        //this.props.history.push('/admindashboard')
        this.props.navigate('/userdashboard');
    }

    render() {
        return (
            <div class="bg-info">
                <br />
                <nav className="navbar navbar-expand-md navbar-dark bg-success">
                            <div class="nav navbar-nav mx-auto navbar-left">
                                <a class="navbar-brand mx-auto nav navbar-nav navbar-right" href="#">BEDAZZLING</a>
                            </div>
                            <div class="nav navbar-nav mx-auto navbar">
                                <a class="navbar-brand mx-auto nav navbar-nav navbar" href="#" onClick={this.userDashboardButton}>USER DASHBOARD</a>
                            </div>
                            <div class="nav mx-auto navbar-nav navbar-right order-3">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item"><a class="nav-link" href="/products" onClick={this.viewProducts}>VIEW PRODUCTS </a></li>
                                   
                                    <li class="nav-item"><a class="nav-link" href="#about" onClick={this.useLogOutButton}> LOG-OFF </a></li>
                                </ul>
                            </div>
                        </nav>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </div>
        )
    }
}
function WithNavigate(props) {
    let navigate = useNavigate();
    return <UserMenuComponent {...props} navigate={navigate} />
  }
export default WithNavigate;