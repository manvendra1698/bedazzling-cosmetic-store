import React, { Component } from 'react'
import axios from 'axios';
import AuthService from '../services/AuthService';
import AdminMenuComponent from './AdminMenuComponent';
import { BrowserRouter, Route } from "react-router-dom";
import {useNavigate} from 'react-router-dom';
import './design.css';

class LoginUserComponent extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
        userName:'',
        password:''
      }
      this.changeHandler=this.changeHandler.bind(this);
      this.login=this.login.bind(this);
    }

    changeHandler(event){

      this.setState({[event.target.name]:event.target.value});
      // console.log(this.state.user.userName);
  
  }
    
    login(event){
      
      let userDet={
          userName:this.state.userName,
          password:this.state.password,
      }
      console.log(userDet);
     axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
     //axios.post('http://localhost:8081/authentication/login',userDet).then(res=>{
      AuthService.login(userDet).then(res=>{
        sessionStorage.setItem("token", res.data)
        console.log(res);
        AuthService.getRole(userDet.userName,userDet.password)
        .then(res=>{console.log(res.data);
          sessionStorage.setItem("role", res.data)});
          if(sessionStorage.getItem("role")==="Admin"){
            console.log("ll");
            this.props.navigate('/admindashboard');
          }else{
            console.log("ll");
            this.props.navigate('/userdashboard');
          }

      },error => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        
        });
     axios.post("http://localhost:8081/authentication/login",userDet).then(res=>{console.log(res.data)});
      event.preventDefault();

    }
  render() {
    return (
      <div className='Bg-app'>
        <div className='content'>
                <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3" style={{"color": "black", "backgroundColor": "#419064"}}>
                                <h3 className="text-center">Login - Sign In</h3>
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> User Name:</label>
                                            <input placeholder="Enter User Name" name="userName" className="form-control"  onChange={this.changeHandler}/>
                                        </div>

                                        <div className = "form-group">
                                            <label> Password: </label>
                                            <input placeholder="Enter Password" name="password" className="form-control"  onChange={this.changeHandler}/>
                                        </div>
                                        <div className="text-center">
                                          <a href="/admindashboard">
                                          <button className="btn btn-primary" style={{float:"left"}} onClick={this.login} >Login</button>
                                        </a>
                                        <a style={{float:"right", color: "black"}}href="/">New User?</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        </div>
            </div>
            </div>
    )
  }
}
function WithNavigate(props) {
  let navigate = useNavigate();
  return <LoginUserComponent {...props} navigate={navigate} />
}
export default WithNavigate;
