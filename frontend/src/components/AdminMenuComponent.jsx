import React, { Component } from 'react'
import {useNavigate} from 'react-router-dom';
class AdminMenuComponent extends Component {

    useLogOutButton = (p) => {
        p.preventDefault();
        //this.props.history.push('/');
        this.props.navigate('/login');
    }


    viewProducts =(p)=>{
        p.preventDefault();
       //this.props.history.push('/books');
       this.props.navigate('/products');
    }
    adminDashboardButton = (p) => {
        p.preventDefault();
        //this.props.history.push('/admindashboard')
        this.props.navigate('/admindashboard');
    }

    render() {
        return (
            <div>
                <br />
                <nav className="navbar fixed-top navbar-expand-lg navbar-dark bg-success">
                            <div class="nav navbar-nav mx-auto navbar-left">
                                <a class="navbar-brand mx-auto nav navbar-nav navbar-right" href="#">BEDAZZLING</a>
                            </div>
                            <div class="nav navbar-nav mx-auto navbar">
                                <a class="navbar-brand mx-auto nav navbar-nav navbar" href="#" onClick={this.adminDashboardButton}>ADMIN DASHBOARD</a>
                            </div>
                            <div class="nav mx-auto navbar-nav navbar-right order-3">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item"><a class="nav-link" href="/products" onClick={this.viewProducts}>VIEW PRODUCTS </a></li>
                                    {/* <li class="nav-item"><a class="nav-link" href="#" onClick={this.viewMembers}>VIEW MEMBERS</a></li> */}
                                    <li class="nav-item"><a class="nav-link" href="#about" onClick={this.useLogOutButton}> LOG-OFF </a></li>
                                </ul>
                            </div>
                        </nav>
                
            </div>
        )
    }
}
function WithNavigate(props) {
    let navigate = useNavigate();
    return <AdminMenuComponent {...props} navigate={navigate} />
  }
export default WithNavigate;