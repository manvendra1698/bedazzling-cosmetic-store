import axios from 'axios';
import React, { Component } from 'react'
import './design.css';


class RegisterUserComponent extends Component {
    constructor(props) {
      super(props)
    
      this.state ={
            
                userId:0,
            userName:'',
            password:'',
            role:{
                roleId:0,
                roleName:''
            }
        }

        
      
      this.changeHandler=this.changeHandler.bind(this);
    }
    changeHandler(event){

        this.setState({[event.target.name]:event.target.value});
    
    }
    submitData=(event)=>{
        console.log(this.state.user);
        let userDet={
            userId:this.state.userId,
            userName:this.state.userName,
            password:this.state.password,
            role:{
                roleId:this.state.roleId,
                roleName:this.state.roleName
            }
        }
        console.log(userDet);
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.post('http://localhost:8081/authentication/users',userDet);
        event.preventDefault();
    }

  render() {
    return (
        <div className='Bg-app'>
        <div className='content'>
                <br></br>
                   <div className = "container" >
                    
                        <div className = "row">
                        
                            <div className = "card col-md-6 offset-md-3 offset-md-3" style={{"color": "black", "backgroundColor": "#419064"}}>
                            
                                <h3 className="text-center">Registration - Sign Up</h3>
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> User Name: </label>
                                            <input placeholder="Enter User Name" name="userName" className="form-control" onChange={this.changeHandler}/>
                                        </div>

                                        <div className = "form-group">
                                            <label> Password: </label>
                                            <input placeholder="Enter Password" name="password" className="form-control" onChange={this.changeHandler}/>
                                        </div>
                                        {/* <div className = "form-group">
                                            <label> Role Id: </label>
                                            <input placeholder="Role Id" name="roleId" className="form-control" onChange={this.changeHandler}/>
                                        </div> */}
                                        <div className = "form-group">
                                            <label> Role Name: </label>
                                            <input placeholder="Enter Role Name" name="roleName" className="form-control" onChange={this.changeHandler}/>
                                        </div>
                                        <div className="text-center">
                                        <button className="btn btn-primary"style={{float:"left"}} onClick={this.submitData}>Sign Up</button>
                                        <a style={{float:"right", color:"black"}}href="/login">Already registered?</a>
                                        </div>
                                    </form>
                                </div>
                                </div> 
                            </div>
                            
                        </div>
                        
                   </div>
            </div>
            
    )
  }
}

export default RegisterUserComponent
