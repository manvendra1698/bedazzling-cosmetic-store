import React, { Component } from 'react';
import {useNavigate} from 'react-router-dom';
import ProductService from '../services/ProductService';
import "./design.css";

class UpdateProductComponent extends Component {

    constructor(props){
        super(props)
        this.state={
            productName: '',
            productType: '',
            productPrice: ''
        }

        this.changeProductNameHandler = this.changeProductNameHandler.bind(this);
        this.changeProductTypeHandler = this.changeProductTypeHandler.bind(this);
        this.changeproductPriceHandler = this.changeproductPriceHandler.bind(this);
        this.saveProduct = this.saveProduct.bind(this);
    }

    changeProductNameHandler= (event) => {
        this.setState({productName: event.target.value});
    }

    changeProductTypeHandler= (event) => {
        this.setState({productType: event.target.value});
    }

    changeproductPriceHandler= (event) => {
        this.setState({productPrice: event.target.value});
    }

    saveProduct= (p) => {
        p.preventDefault();
        let product = {productName: this.state.productName, productType: this.state.productType, productPrice: this.state.productPrice};
        console.log('product => ' + JSON.stringify(product));

        ProductService.addProduct(product).then(res =>{
            this.props.navigate('/products');
        });

    }

    cancel(){
        this.props.navigate('/products');
    }

    render() {
        return (
            <div className='add-product'>
                <br></br>
                <div className='content'>
                <div className='container'>
                    <div className='row'>
                        <div className='card col-md-6 offset-md-3 offset-md-3' style={{"color": "black", "backgroundColor": "#419064"}}>
                            <h3 className='text-center'>Add Product</h3>
                            <div className='card-body'>
                                <form action="">
                                    <div className='form-group'>
                                        <label htmlFor="">Product Name: </label>
                                        <input placeholder='Product Name' name='productName' className='form-control' value={this.state.productName} onChange={this.changeProductNameHandler}/>
                                    </div>

                                    <div className='form-group'>
                                        <label htmlFor="">Product Type </label>
                                        <input placeholder='Product Type' name='productType' className='form-control' value={this.state.productType} onChange={this.changeProductTypeHandler}/>
                                    </div>

                                    <div className='form-group'>
                                        <label htmlFor="">Product Price: </label>
                                        <input placeholder='Product Price' name='productPrice' className='form-control' value={this.state.productPrice} onChange={this.changeproductPriceHandler}/>
                                    </div>

                                    <button className="btn btn-primary" onClick={this.saveProduct}>Save</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
                </div>
            </div>
        );
    }
}

function WithNavigate(props) {
    let navigate = useNavigate();
    return <UpdateProductComponent {...props} navigate={navigate} />
}

export default WithNavigate;