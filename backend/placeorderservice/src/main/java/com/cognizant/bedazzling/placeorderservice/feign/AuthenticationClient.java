package com.cognizant.bedazzling.placeorderservice.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "authentication-service", url = "${AUTHENTICATION_SERVICE:http://localhost:8081}")
public interface AuthenticationClient {
	@GetMapping("/authentication/validate")
	public boolean validate(@RequestHeader(name = "Authorization") String token);
}
