package com.cognizant.bedazzling.placeorderservice.exception;

public class OrderNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * 
	 * @param message
	 */
	public OrderNotFoundException(String message) {
		super(message);
	}
 

}
