package com.cognizant.bedazzling.placeorderservice.repository;

import com.cognizant.bedazzling.placeorderservice.model.Orders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdersRepo extends JpaRepository<Orders,Integer> {

}
