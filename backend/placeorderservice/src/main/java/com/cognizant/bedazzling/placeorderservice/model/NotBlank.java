package com.cognizant.bedazzling.placeorderservice.model;

public @interface NotBlank {
    String message();
}
