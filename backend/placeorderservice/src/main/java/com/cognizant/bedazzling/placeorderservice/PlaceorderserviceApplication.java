package com.cognizant.bedazzling.placeorderservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class PlaceorderserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlaceorderserviceApplication.class, args);
	}

}
