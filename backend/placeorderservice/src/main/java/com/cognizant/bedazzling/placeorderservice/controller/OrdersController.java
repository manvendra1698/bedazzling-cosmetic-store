package com.cognizant.bedazzling.placeorderservice.controller;

import com.cognizant.bedazzling.placeorderservice.model.Orders;
import com.cognizant.bedazzling.placeorderservice.service.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrdersController {
	@Autowired
	OrderServiceImpl orderServiceImpl;

	@PostMapping("/cart")
	public ResponseEntity<String> addOrders(@RequestBody Orders orders) {
		orderServiceImpl.addOrders(orders);
		return new ResponseEntity<String>("Success", HttpStatus.OK);
	}
	@GetMapping("cart/{id}")
	public Orders getOrderById(@PathVariable int id){
		return orderServiceImpl.getOrderById(id);
	}
}
