package com.cognizant.bedazzling.placeorderservice.exception;

public class OrderAlreadyExistsException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * 
	 * @param message
	 */
	public OrderAlreadyExistsException(String message) {
		super(message);
	}

}
