package com.cognizant.bedazzling.placeorderservice.service;

import com.cognizant.bedazzling.placeorderservice.model.Orders;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface OrderService {
	void addOrders(Orders orders);

	public Orders getOrderById(long productId);


}
