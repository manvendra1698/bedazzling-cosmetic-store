package com.cognizant.bedazzling.placeorderservice.model;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Orders {
	@Id
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")

	private Integer orderId;
	// @NotBlank(message="Id can not be empty")
	// @NotNull
	private Integer customerId;
	// @NotBlank(message="Id can not be empty")
	@NotNull

	private Integer productId;

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductIid(Integer productId) {
		this.productId = productId;
	}

//	public String getOrderAddress() {
//		return orderAddress;
//	}
//
//	public void setOrderAddress(String orderAddress) {
//		this.orderAddress = orderAddress;
//	}
//
//	public Double getProductPrice() {
//		return productPrice;
//	}
//
//	public void setProductPrice(Double productPrice) {
//		this.productPrice = productPrice;
//	}
//
//	public Double getCartTotal() {
//		return cartTotal;
//	}
//
//	public void setCartTotal(Double cartTotal) {
//		this.cartTotal = cartTotal;
//	}
}
