package com.cognizant.bedazzling.placeorderservice.service;

import com.cognizant.bedazzling.placeorderservice.exception.ResourceNotFoundException;
import com.cognizant.bedazzling.placeorderservice.model.Orders;
import com.cognizant.bedazzling.placeorderservice.repository.OrdersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@Service
public class OrderServiceImpl implements OrderService
{
	@Autowired
	OrdersRepo ordersRepo;
	private Scanner OrdersRepo;

	@Override
	public void addOrders(Orders orders) {
		ordersRepo.save(orders);
	}

	@Override
	public Orders getOrderById(long productId)
	{
		return ordersRepo.findById((int) productId).orElse(null);
	}
}
