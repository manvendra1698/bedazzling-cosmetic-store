package com.cognizant.bedazzling.productservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cognizant.bedazzling.productservice.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
	@Query(value = "select * from product where price between ?1 and ?2", nativeQuery = true)
	public Product getProduct(int startPrice, int endPrice);

}
