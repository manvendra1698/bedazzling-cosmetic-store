package com.cognizant.bedazzling.productservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cognizant.bedazzling.productservice.model.Cart;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {

}
