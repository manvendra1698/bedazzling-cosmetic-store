package com.cognizant.bedazzling.productservice.exception;

public class ProductAlreadyExistsException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * 
	 * @param message
	 */
	public ProductAlreadyExistsException(String message) {
		super(message);
	}

}
