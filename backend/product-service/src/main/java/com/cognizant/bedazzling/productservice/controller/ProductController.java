package com.cognizant.bedazzling.productservice.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.bedazzling.productservice.exception.ProductNotFoundException;
import com.cognizant.bedazzling.productservice.feign.AuthenticationClient;
import com.cognizant.bedazzling.productservice.model.Product;
import com.cognizant.bedazzling.productservice.service.ProductService;
import com.cognizant.bedazzling.productservice.service.ProductServiceImpl;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@CrossOrigin
public class ProductController {
	private static Logger log = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	ProductServiceImpl productService;

	@Autowired
	AuthenticationClient authenticationClient;

	@PostMapping("/products")
	// @ApiOperation(value = "addProduct", notes = "add a new product ", httpMethod
	// = "POST", response = ResponseEntity.class)
	public ResponseEntity<String> addProduct(@RequestBody Product product,
			@RequestHeader(name = "Authorization") String token) {
		if (authenticationClient.validate(token)) {
			if (productService.addProduct(product)) {
				log.debug("Product added successfully");
				return new ResponseEntity<>("Product Added", HttpStatus.CREATED);
			} else {
				log.debug("Product added unsuccessful");

				return new ResponseEntity<>("product not added", HttpStatus.BAD_REQUEST);
			}
		} else {
			return new ResponseEntity<>("", HttpStatus.FORBIDDEN);
		}

	}

	@GetMapping("/products")
	// @ApiOperation(value = "getProduct", notes = "get the details of products",
	// httpMethod = "GET", response = ResponseEntity.class)
	public List<Product> getProduct(@RequestHeader(name = "Authorization") String token) {
		if (authenticationClient.validate(token)) {
			return productService.getProduct();
		} else {
			throw new ProductNotFoundException("Cannot find the product: ");
		}
	}

	@GetMapping("/product/{id}")
	// @ApiOperation(value = "getProduct", notes = "get product details ",
	// httpMethod = "GET", response = ResponseEntity.class)
	public ResponseEntity<Product> getProductById(@PathVariable("id") long id,
			@RequestHeader(name = "Authorization") String token) throws ProductNotFoundException {
		ResponseEntity<Product> responseEntity;

		if (authenticationClient.validate(token)) {
			Product product = productService.getProductById(id);
			if (product != null) {
				responseEntity = new ResponseEntity<Product>(product, HttpStatus.FOUND);
			} else {
				throw new ProductNotFoundException("product not found");
			}
		} else {
			responseEntity = new ResponseEntity<Product>(new Product(), HttpStatus.FORBIDDEN);
		}
		return responseEntity;
	}

	@PutMapping("/product/{id}")
	// @ApiOperation(value = "updateProductbyId", notes = "update product details ny
	// Id ", httpMethod = "PUT", response = ResponseEntity.class)
	public ResponseEntity<String> updateProduct(@RequestBody Product product,
			@RequestHeader(name = "Authorization") String token) throws ProductNotFoundException {

		if (authenticationClient.validate(token)) {

			if (productService.updateProduct(product)) {
				return new ResponseEntity<>("Product Updated", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Product Not Updated", HttpStatus.BAD_REQUEST);
			}
		} else {
			return new ResponseEntity<>("", HttpStatus.FORBIDDEN);
		}
	}

	@DeleteMapping("/product/{id}")
	// @ApiOperation(value = "deleteProduct", notes = "delete product details ",
	// httpMethod = "DELETE", response = ResponseEntity.class)
	public ResponseEntity<String> deleteProduct(@RequestHeader(name = "Authorization") String token,
			@PathVariable long id) {
		if (authenticationClient.validate(token)) {
			if (productService.deleteProduct(id)) {
				return new ResponseEntity<>("Product Updated", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Product Not Updated", HttpStatus.BAD_REQUEST);
			}
		} else {
			return new ResponseEntity<>("", HttpStatus.FORBIDDEN);
		}
	}

}
