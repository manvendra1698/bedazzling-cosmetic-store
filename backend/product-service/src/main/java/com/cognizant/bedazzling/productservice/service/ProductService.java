package com.cognizant.bedazzling.productservice.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cognizant.bedazzling.productservice.model.Product;

@Service
public interface ProductService {

	public boolean addProduct(Product product);

	public List<Product>  getProduct();

	public Product getProductById(long id);

	public boolean updateProduct(Product product);

	public boolean deleteProduct(long id);

	/*public static List<Product> getAllProducts() {
		// TODO Auto-generated method stub
		return null;
	}*/

	

}
