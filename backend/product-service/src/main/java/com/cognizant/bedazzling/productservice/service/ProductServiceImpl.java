package com.cognizant.bedazzling.productservice.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.bedazzling.productservice.controller.ProductController;
import com.cognizant.bedazzling.productservice.exception.ProductAlreadyExistsException;
import com.cognizant.bedazzling.productservice.exception.ProductNotFoundException;
import com.cognizant.bedazzling.productservice.model.Product;
import com.cognizant.bedazzling.productservice.repository.ProductRepository;

import lombok.extern.slf4j.Slf4j;

@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	ProductRepository productRepository;

	@Override
	public boolean addProduct(Product product) {
		// TODO Auto-generated method stub
		boolean ans = false;
		boolean status = productRepository.existsById(product.getProductId());
		if (!status) {
			productRepository.save(product);
			ans = true;
		} else
			throw new ProductAlreadyExistsException("Product already Exists");
		return ans;
	}

	@Override
	public List<Product> getProduct() {
		// TODO Auto-generated method stub
		return productRepository.findAll();
	}

	@Override
	public Product getProductById(long id) {

		Product product;
		Optional<Product> op = productRepository.findById(id);
		product = op.get();
		if (product == null) {
			throw new ProductNotFoundException("No such product");
		}
		return product;
	}

	@Override
	public boolean updateProduct(Product product) {
		// TODO Auto-generated method stub
		boolean ans = false;
		boolean status = productRepository.existsById(product.getProductId());
		if (status) {
			productRepository.save(product);
			ans = true;
		} else {
			throw new ProductNotFoundException("No such book");
		}
		return ans;
	}

	@Override
	public boolean deleteProduct(long id) {
		// TODO Auto-generated method stub
		boolean ans = false;
		Product product;
		Optional<Product> op = productRepository.findById(id);
		product = op.get();
		if (product == null) {
			throw new ProductNotFoundException("No such book");
		} else {
			productRepository.deleteById(id);
			ans = true;
		}
		return ans;
	}

	/*
	 * @Override public List<Product> getProduct() { // TODO Auto-generated method
	 * stub return null; }
	 */

}
