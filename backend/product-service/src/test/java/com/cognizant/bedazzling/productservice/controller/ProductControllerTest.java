package com.cognizant.bedazzling.productservice.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.cognizant.bedazzling.productservice.exception.ProductNotFoundException;
import com.cognizant.bedazzling.productservice.feign.AuthenticationClient;
import com.cognizant.bedazzling.productservice.model.Product;
import com.cognizant.bedazzling.productservice.service.ProductService;
import com.cognizant.bedazzling.productservice.service.ProductServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {
	@MockBean
	ProductServiceImpl productService;

	@MockBean
	AuthenticationClient authClient;

	ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private MockMvc mockMvc;

	// private Product product;

	/*
	 * @Before public void setUp() throws Exception {
	 * 
	 * product = new Product(); when(productService.getProductById((long)
	 * 1).thenReturn(product); when(productService.getProductById((long)
	 * 2).thenThrow(ProductNotFoundException.class); }
	 */

	@Test
	public void testGetProductById() throws Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/product/1").header("Authorization")
				.accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		Product rProduct = mapper.readValue(result.getResponse().getContentAsString(), Product.class);
		assertEquals(productService.toString(), rProduct.toString());
	}

	@Test
	public void testGetProductByIdProductNotFoundException() throws Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/product/2").accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		assertEquals(404, result.getResponse().getStatus());
	}

}
