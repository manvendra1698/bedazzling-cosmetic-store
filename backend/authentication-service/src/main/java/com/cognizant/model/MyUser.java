package com.cognizant.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Model class for storing user data in database
 *
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "users")
public class MyUser {

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int userId;

	@Column(name = "name")
	// @NotBlank(message = "User name cannot be empty")
	private String userName;

	// @NotBlank(message = "Password cannot be blank")
	// @Pattern(regexp = "^[A-Za-z0-9]+$")
	private String password;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "roleId")
	private Role role;

	public MyUser(int i, String string, String string2) {
		// TODO Auto-generated constructor stub
	}

	public MyUser() {
		// TODO Auto-generated constructor stub
	}

	public String getUserName() {
		// TODO Auto-generated method stub
		return userName;
	}

	public String getPassword() {
		// TODO Auto-generated method stub
		return password;
	}

	// public void setUserId(int i) {
	// TODO Auto-generated method stub

	// }

	// public void setUserName(String string) {
	// TODO Auto-generated method stub

	// }

	// public void setPassword(String string) {
	// TODO Auto-generated method stub

	// }

	public Integer getUserId() {
		// TODO Auto-generated method stub
		return userId;
	}

}
