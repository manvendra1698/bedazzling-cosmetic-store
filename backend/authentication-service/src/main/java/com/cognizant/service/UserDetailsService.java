package com.cognizant.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cognizant.model.MyUser;

@Service
public interface UserDetailsService {

	public void addMyUser(MyUser myuser);

	public List<MyUser> getAllMyUser();

	public MyUser getMyUser(String id);

	public MyUser updateMyUser(MyUser myuser);

	public MyUser deleteMyUser(MyUser myuser);

}
