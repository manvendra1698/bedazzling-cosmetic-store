package com.cognizant.exception;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * This class is for customizing the exception handler
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CustomErrorResponse {

	private String message;
	private LocalDateTime dateTime;
	public CustomErrorResponse(String string, LocalDateTime now) {
		// TODO Auto-generated constructor stub
	}
	public CustomErrorResponse() {
		// TODO Auto-generated constructor stub
	}
	public void setDateTime(LocalDateTime now) {
		// TODO Auto-generated method stub
		
	}
	public void setMessage(String message2) {
		// TODO Auto-generated method stub
		
	}
	public String getMessage() {
		// TODO Auto-generated method stub
		return message;
	}
	public LocalDateTime getDateTime() {
		// TODO Auto-generated method stub
		return dateTime;
	}
}
